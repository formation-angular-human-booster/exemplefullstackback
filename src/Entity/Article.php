<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ApiResource(
 *     denormalizationContext={"groups"={"postArticleWithCateg"}},
 *     normalizationContext={"groups"={"getArticleWithCateg"}})
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"getArticleWithCateg","postArticleWithCateg"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"getArticleWithCateg","postArticleWithCateg"})
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"getArticleWithCateg","postArticleWithCateg"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="articles", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"getArticleWithCateg","postArticleWithCateg"})
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"getArticleWithCateg","postArticleWithCateg"})
     */
    private $displayOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDisplayOrder(): ?int
    {
        return $this->displayOrder;
    }

    public function setDisplayOrder(int $displayOrder): self
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }
}
